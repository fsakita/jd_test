import React from 'react';
import BookItem from './bookitem';

const Books = ({books}) => {
	console.log(books);

	const stored = JSON.parse(localStorage.getItem("books"));
	let isSelected = " ";

	// Filter only 8 first items
	const booksLen = books.length;
	const featuredBooks  = books.map((book, i) => {
		if(stored !== null){
			isSelected = ( stored.indexOf( book.id ) > -1 ) ? "is-selected" : " ";
		}
		if(i < booksLen - 2){
			return <BookItem
						key={book.id}
						id={book.id}
						title={book.volumeInfo.title}
						cover={book.volumeInfo.imageLinks.thumbnail}
						authors={book.volumeInfo.authors}
						pages={book.volumeInfo.pageCount}
						description={book.volumeInfo.description}
						isSelected={isSelected}
					/>
		}
		return null;
	})

	// Loader
	if(books.length === 0){
		return <div className="loader"><i className="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div>
	}

	return (
		<div className="books">
			{featuredBooks}
		</div>
	)
}

export default Books;