import React, {Component} from 'react';

class Logo extends Component {

	render(){
		return (
			<div className="logo">
				<img className="hide-small" src={require('../assets/logo.png')} alt='logo' />
				<div className="appTitle">The Book Store</div>
			</div>
		)
	}
}

export default Logo;