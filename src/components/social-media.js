import React from 'react';

const SocialMedia = () => {

	return (
		<div className="socialMedia">
			<ul>
				<li><a><i className="fa fa-twitter fa-3x" aria-hidden="true"></i></a></li>
				<li><a><i className="fa fa-facebook-square fa-3x" aria-hidden="true"></i></a></li>
				<li><a><i className="fa fa-instagram fa-3x" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	)
}

export default SocialMedia;