import React from 'react';

const Description = () => {

	const description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non orci nec nunc lobortis sodales. Fusce scelerisque iaculis nibh, non venenatis dolor blandit a. Integer maximus neque sed massa sollicitudin cursus. Nunc et elementum velit, quis maximus augue. Ut mattis vel erat et iaculis.";

	return (
		<div className="pageDescription">
			<p>
				{description}
			</p>
		</div>
	)
}

export default Description;