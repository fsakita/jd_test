import React from 'react';
import BookItem from './bookitem';

const BookFeatured = ({books, conditional}) => {
	console.log(books);

	const stored = JSON.parse(localStorage.getItem("books"));
	let isSelected = " ";

	// Filter only 2 last items
	const booksLen = books.length;
	const featuredBooks  = books.map((book, i) => {
		if(stored !== null){
			isSelected = ( stored.indexOf( book.id ) > -1 ) ? "is-selected" : " ";
		}
		const comp = i >= booksLen - 2;
		if(comp){
			return <BookItem
						key={book.id}
						id={book.id}
						title={book.volumeInfo.title}
						cover={book.volumeInfo.imageLinks.thumbnail}
						authors={book.volumeInfo.authors}
						pages={book.volumeInfo.pageCount}
						description={book.volumeInfo.description}
						isSelected={isSelected}
					/>
		}
		return null;
	})

	// Loader
	if(books.length === 0){
		return null
	}

	return (
		<div className="bookFeatured">
			<h2>Featured</h2>
			{featuredBooks}
		</div>
	)
}

export default BookFeatured;