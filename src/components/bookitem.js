import React, {Component} from 'react';

class BookItem extends Component {
	constructor(props){
		super(props);

		this.state = {
			"selectedBook": " "
		}
	}

	componentDidMount(){
		const {isSelected} = this.props;
		this.setState({"selectedBook": isSelected});
	}

	toggleSelection(){
		const {selectedBook} = this.state;
		const {id} = this.props;
		var stored = JSON.parse(localStorage.getItem("books"));
		
		// Local Storage Selected Books
		if(selectedBook === " "){
			if(stored){
				stored.push(id);
			} else {
				stored = [id];
			}
		} else {
			stored = stored.filter(item => item !== id)
		}

		localStorage.setItem("books", JSON.stringify(stored));

		// Update css on selected Book
		const css = (selectedBook === " ") ? "is-selected" : " ";
    	this.setState({"selectedBook":css});
	}

	render(){
		const {title, cover, authors, description, pages} = this.props;

		// Book description max 140 characters
		const shortDescription = description.length > length ? description.substring(0, 137) + "..." : description.substring(0, 140);

		// List authors
		const authorLen = authors.length;
		const authorList = authors.map((author, i) => {
			if(i < authorLen - 1){
				return author + ", "
			}
			return author
		})

		return (
			<div className={"bookItem " + this.state.selectedBook} onClick={this.toggleSelection.bind(this)}>
				<div className={"bookItem-content " + this.state.selectedBook}>
					<img src={cover} alt={title} />
					<div className="bookItem-info">
						<h3>{title}</h3>
						<p className="authors">{authorList}</p>
						<p className="pages">Pages: {pages}</p>
						<p className="description">{shortDescription}</p>
					</div>
				</div>
			</div>
		)
	}
}

export default BookItem;