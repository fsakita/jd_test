import React, {Component} from 'react';
import Description from './description';

class Menu extends Component{
	constructor(props){
		super(props);

		this.state = {
			"selectedMenu": "hide-small"
		}
	}

	// Open mobile menu
	toggleMenu(){
		console.log(this.state);
		const {selectedMenu} = this.state;
		const css = (selectedMenu === "hide-small") ? " " : "hide-small";
    	this.setState({"selectedMenu":css});
	}

	render(){
		const menuItems = [
			'Home',
			'Books',
			'Magazines',
			'E-books',
			'Account'
		]

		const menu = menuItems.map((menuItem) => {
			return <li key={menuItem}><a href="#">{menuItem}</a></li>
		})

		const {selectedMenu} = this.state;

		return (
			<div>
				<div className="menu">
					<div className="burgerMenu hide-large hide-medium" onClick={this.toggleMenu.bind(this)}>
						<i className="fa fa-bars fa-3x" aria-hidden="true"></i>
					</div>
					<ul className={selectedMenu}>
						{menu}
					</ul>
				</div>
				<Description />
			</div>
		)
	}
}

export default Menu;