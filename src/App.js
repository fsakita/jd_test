import React, { Component } from 'react';
import './App.css';

import Logo from './components/logo';
import Menu from './components/menu';
import Books from './components/books';
import BooksFeatured from './components/books-featured';
import SocialMedia from './components/social-media';

class App extends Component {
  constructor(props){
    super(props);

    this.state ={
      books: []
    }
  }

  componentDidMount(){
    // Getting data from GoogleAPI
    fetch('https://www.googleapis.com/books/v1/volumes?q=HTML5')
      .then(response => response.json())
      .then(result => this.onSetResult(result));

  }
  // Set data to state
  onSetResult = (result) => {
    this.setState({books: result.items});
  }



  render() {
    const {books} = this.state;
    return (
      <div className="container">
        <div className="row">
          <div className="c-100">
            <Logo />
          </div>
        </div>
        <div className="row">
          <div className="c-100">
            <Menu />
          </div>
        </div>
        <div className="row bookColumn">
          <div className="c-70">
            <Books books={books} />
          </div>
          <div className="c-30">
            <BooksFeatured books={books} />
          </div>
        </div>
        <SocialMedia />
      </div>
    );
  }
}

export default App;
