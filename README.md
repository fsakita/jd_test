Fabio Sakita - JD Test

Install
1. clone repo
2. npm install
3. npm start

Create production build
1. npm run build

Project
To build the project I chose a simple React application. I tried to keep it as simple as possible and functional. Working with ES6+, focusing on a modular and clean code.

Selected books are stored at local storage so data is not lost when reloading the page.

3 viewports: 
 - Small (max-width 600px);
 - Medium (601px to 991px);
 - Large (min-width 992px);

The page should run without any issues from IE9+, Google Chrome, Safari, Firefox and Opera.